package autowire

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestBeanFactoryAutowireByType(t *testing.T) {
	sa := &ServiceA{"sa"}
	sb := &ServiceB{}
	sc := &ServiceC{}

	bf := NewBeanFactory()
	bf.RegisterBeans(sa, sb, sc)
	err := bf.Autowire()

	require.NoError(t, err)

	require.True(t, sc.Sa == sa)
	require.True(t, sc.Sb == sb)
}

func TestBeanFactoryAutowireDuplicatedBean(t *testing.T) {
	sa := &ServiceA{"sa"}
	sa2 := &ServiceA{"sa2"}
	sb := &ServiceB{}
	sc := &ServiceC{}

	bf := NewBeanFactory()
	// duplicated ServiceA
	bf.RegisterBeans(sa, sa2, sb, sc)
	err := bf.Autowire()
	require.ErrorContains(t, err, "find more than one bean by type:IServiceA")
}

func TestBeanFactoryAutowireByName(t *testing.T) {
	sa := &ServiceA{"sa"}
	sa2 := &ServiceA{"sa2"}
	sb := &ServiceB{}
	sc := &ServiceC{}

	bf := NewBeanFactory()
	bf.RegisterBeans(sa, sa2, sb, sc)
	bf.RegisterBeanByName("sa2", sa2)
	err := bf.Autowire()
	require.NoError(t, err)

	require.True(t, sc.Sa == sa2)
	require.True(t, sc.Sb == sb)
}

func TestBeanFactoryAutowireNotNilField(t *testing.T) {
	sa := &ServiceA{"sa"}
	sb := &ServiceB{}
	sa2 := &ServiceA{"sa2"}
	sc := &ServiceC{
		Sa: sa2, // will not autowire this field, because you have set the value
	}

	bf := NewBeanFactory()
	bf.RegisterBeans(sa, sb, sc)
	err := bf.Autowire()
	require.NoError(t, err)

	require.True(t, sc.Sa == sa2)
	require.True(t, sc.Sb == sb)
}

func TestBeanFactoryAutowireByCreateBean(t *testing.T) {
	sb := &ServiceB{}
	sc := &ServiceC{}

	bf := NewBeanFactory()
	bf.RegisterBeans(sb, sc)

	var sa IServiceA

	bf.RegisterBeanCreatorByType(GetReflectType[IServiceA](), func(bf BeanFactory) any {
		sa = &ServiceA{"sa"}
		return sa
	})
	err := bf.Autowire()
	require.NoError(t, err)

	require.NotNil(t, sa)
	require.True(t, sc.Sa == sa)
	require.True(t, sc.Sb == sb)
}

func TestBeanFactoryBeanCreator(t *testing.T) {
	bf := NewBeanFactory()
	beanType := GetReflectType[*ServiceA]()
	var sa IServiceA
	bf.RegisterBeanCreatorByType(beanType, func(bf BeanFactory) any {
		sa = &ServiceA{"sa"}
		return sa
	})

	bean, err := bf.GetBeanByType(GetReflectType[*ServiceA]())
	require.NoError(t, err)

	require.NotNil(t, sa)
	require.NotNil(t, bean)
	require.True(t, bean == sa)

}

func TestBeanFactoryBeanByType(t *testing.T) {
	bf := NewBeanFactory()
	var sa IServiceA

	RegisterBeanCreatorByType[*ServiceA](bf, func(bf BeanFactory) any {
		sa = &ServiceA{"sa"}
		return sa
	})

	bean, err := GetBeanByType[*ServiceA](bf)
	require.NoError(t, err)

	require.NotNil(t, sa)
	require.NotNil(t, bean)
	require.True(t, bean == sa)

	beans := GetBeansByType[IServiceA](bf)
	require.Equal(t, 1, len(beans))
	require.True(t, beans[0] == sa)

}

func TestGetReflectType(t *testing.T) {
	tp := GetReflectType[ServiceA]()
	tpE := reflect.TypeOf(ServiceA{})
	require.Equal(t, tpE, tp)

	tp = GetReflectType[IServiceA]()
	tpE = reflect.TypeOf((*IServiceA)(nil)).Elem()
	require.Equal(t, tpE, tp)
}

type IServiceA interface {
	DoServiceA() error
}

type IServiceB interface {
	DoServiceB() error
}

type IServiceC interface {
	DoServiceC() error
}

// ServiceA
type ServiceA struct {
	name string
}

func (s *ServiceA) DoServiceA() error {
	fmt.Println("ServiceA[" + s.name + "] DoServiceA")
	return nil
}

// ServiceB
type ServiceB struct {
}

func (s *ServiceB) DoServiceB() error {
	fmt.Println("ServiceB DoServiceB")
	return nil
}

// ServiceC
type ServiceC struct {
	Sa IServiceA `bean:"sa2"` // autowire by name first
	Sb IServiceB // autowire by type
}

func (s *ServiceC) DoServiceC() error {
	fmt.Println("ServiceC DoServiceC")
	err := s.Sa.DoServiceA()
	if err != nil {
		return err
	}
	err = s.Sb.DoServiceB()
	return err
}
