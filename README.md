Dependency Injection for Go
==================================================
It is a very simple dependency injection component for go. 

## Usage
### 1、Define interface
```go
type IServiceA interface {
    DoServiceA() error
}

type IServiceB interface {
    DoServiceB() error
}

type IServiceC interface {
    DoServiceC() error
}
```
### 2、Implements interface
```go
// ServiceA
type ServiceA struct {
	name string
}

func (s *ServiceA) DoServiceA() error {
	fmt.Println("ServiceA DoServiceA")
	return nil
}

// ServiceB
type ServiceB struct {
}

func (s *ServiceB) DoServiceB() error {
	fmt.Println("ServiceB DoServiceB")
	return nil
}

// ServiceC
type ServiceC struct {
	Sa IServiceA `bean:"sa"` // autowire by name first
	Sb IServiceB // autowire by type
}

func (s *ServiceC) DoServiceC() error {
	fmt.Println("ServiceC DoServiceC")
	err := s.Sa.DoServiceA()
	if err != nil {
		return err
	}
	err = s.Sb.DoServiceB()
	return err
}
```
### 3、Autowire beans
```go
func main() {
    sa := &ServiceA{}
    sb := &ServiceB{}
    sc := &ServiceC{}

    // autowire beans
    bf := NewBeanFactory()
    bf.RegisterBeans(sa, sb, sc)
    bf.Autowire()

    sc.DoServiceC()
    // out:
    // ServiceC DoServiceC
    // ServiceA DoServiceA
    // ServiceB DoServiceB
}
```
### 4、A practice sample
* [autowire-sample](https://gitee.com/mqyqingkong/autowire-sample)